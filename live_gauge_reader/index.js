Webcam.set({
    width: 320,
    height: 240,
    image_format: 'jpeg',
    jpeg_quality: 90
});
Webcam.attach('#my_camera');

function onOpenCvReady() {
    old_line = {
        "startPoint": new cv.Point(0, 0),
        "endPoint": new cv.Point(0, 0),
        "colour": new cv.Scalar(255, 255, 0)
    }
    // document.getElementById('status').innerHTML = 'OpenCV.js is ready.';
    Webcam.on('live', function () {
        setInterval(take_snapshot, 200);
    });
}

let readingImgElement = document.getElementById('readingImageSrc');
// readingImgElement.addEventListener('change', (e) => {
//     readingImgElement.src = URL.createObjectURL(e.target.files[0]);
// }, false);
let old_line = {};
let calibrated_values = {};
let final_angle = 0;
let startPoint = {"x": 0, "y": 0};

readingImgElement.onload = function () {
    let mat = cv.imread(readingImgElement);
    // cv.imshow('canvasOutputCalibration', mat);
    calibrate_gauge(mat, 50, 320).then(calibrated_values => {
        if (calibrated_values.circled_detected) {
            console.log("calibrated values: ", calibrated_values);
            calibration_values = {
                "min_angle": 50,
                "max_angle": 320,
                "min_value": 0,
                "max_value": 200,
                "units": "PSI",
                "x": calibrated_values.x,
                "y": calibrated_values.y,
                "r": calibrated_values.r
            };
            get_current_value(calibrated_values.img, calibration_values.min_angle, calibration_values.max_angle, calibration_values.min_value,
                calibration_values.max_value, calibration_values.x, calibration_values.y, calibration_values.r)
                .then(results => {
                    if (results.reading_value != "0") {
                        // cv.line(results.img, results.line.startPoint, results.line.endPoint, results.line.colour);
                        let endP = new cv.Point((calibration_values.x + calibration_values.r*Math.sin((360 - results.final_angle) * (Math.PI / 180))), (calibration_values.y + calibration_values.r*Math.cos((360 - results.final_angle) * (Math.PI / 180))));
                        cv.line(results.img, new cv.Point(calibration_values.x, calibration_values.y), endP, results.line.colour);
                        final_angle = results.final_angle;
                        old_line = results.line;
                        old_line.endPoint = endP;
                        document.getElementById('readingResults').innerHTML = "Results: " + results.reading_value + " " + calibration_values.units;
                    }
                    else {     
                        let endP = new cv.Point((calibration_values.x + calibration_values.r * Math.sin((360 - final_angle) * (Math.PI / 180))), (calibration_values.y + calibration_values.r * Math.cos((360 - final_angle) * (Math.PI / 180))));      
                        cv.line(results.img, new cv.Point(calibration_values.x, calibration_values.y), endP, old_line.colour);
                    }
                    cv.imshow('canvasOutputCalibration', results.img);
                    results.img.delete();
                });
        }
        else{
            old_line = {
                "startPoint": new cv.Point(calibration_values.x, calibration_values.y),
                "endPoint": new cv.Point(0, 0),
                "colour": new cv.Scalar(255, 255, 0)
            }
        }
        // cv.imshow('canvasOutputCalibration', calibrated_values.img);
        // calibrated_values.img.delete();
    });
    // display results in page
}

function take_snapshot() {
    // take snapshot and get image data
    Webcam.snap(function (data_uri) {
        document.getElementById('readingImageSrc').src = data_uri;
        // let mat = cv.imread(document.getElementById('readingImageSrc'));
        // let min_angle = parseInt(document.getElementById("fmin_angle").value);
        // let max_angle = parseInt(document.getElementById("fmax_angle").value);
        // let min_value = parseInt(document.getElementById("fmin_value").value);
        // let max_value = parseInt(document.getElementById("fmax_value").value);
        // let units = document.getElementById("funits").value;
        // cv.imshow('canvasOutputCalibration', mat);
        // calibrate_gauge(mat, -1, -1).then(calibrated_values => {    
        //     console.log("calibrated values: ", calibrated_values);
        //     // calibration_values = {
        //     //   "min_angle": min_angle, 
        //     //   "max_angle": max_angle, 
        //     //   "min_value": min_value, 
        //     //   "max_value": max_value, 
        //     //   "units": units, 
        //     //   "x": calibrated_values.x, 
        //     //   "y": calibrated_values.y, 
        //     //   "r": calibrated_values.r
        //     // };
        //     // let results = "Minimum Angle: " + calibration_values.min_angle + ", Maximum Angle: " + calibration_values.max_angle
        //     // + ", Minimum Value: " + calibration_values.min_value + ", Maximum Value: " + calibration_values.max_value + ", Units: " + calibration_values.units;
        //     // document.getElementById('calibrationResults').innerHTML = results;
        //     cv.imshow('canvasOutputCalibration', calibrated_values.img);
        //     calibrated_values.img.delete();
        //   });
        // display results in page
        // document.getElementById('results').innerHTML = 
        //     '<h2>Here is your image:</h2>' + 
        //     '<img src="'+data_uri+'"/>';
    });
}