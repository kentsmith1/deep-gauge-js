Webcam.set({
    width: 320,
    height: 240,
    image_format: 'jpeg',
    jpeg_quality: 90
});
Webcam.attach('#my_camera');

function take_snapshot(name) {
    console.log(name);
    // take snapshot and get image data
    Webcam.snap(function (data_uri) {
        // display results in page      
        document.getElementById(name).innerHTML =
            '<img src="' + data_uri + '"/>';
    });
}

let calibrationImgElement = document.getElementById('calibrationImageSrc');
let calibrationInputElement = document.getElementById('calibrationFileInput');
let calibration_values = {};
calibrationInputElement.addEventListener('change', (e) => {
    calibrationImgElement.src = URL.createObjectURL(e.target.files[0]);
}, false);

let readingImgElement = document.getElementById('readingImageSrc');
let readingInputElement = document.getElementById('readingFileInput');
readingInputElement.addEventListener('change', (e) => {
    readingImgElement.src = URL.createObjectURL(e.target.files[0]);
}, false);

function calibrateGauge() {
    let mat = cv.imread(calibrationImgElement);
    console.log()
    let min_angle = parseInt(document.getElementById("fmin_angle").value);
    let max_angle = parseInt(document.getElementById("fmax_angle").value);
    let min_value = parseInt(document.getElementById("fmin_value").value);
    let max_value = parseInt(document.getElementById("fmax_value").value);
    let units = document.getElementById("funits").value;
    console.log(min_angle,  max_angle, min_value, max_value, units);
    calibrate_gauge(mat, min_angle, max_angle).then(calibrated_values => {    
      console.log("calibrated values: ", calibrated_values);
      calibration_values = {
        "min_angle": min_angle, 
        "max_angle": max_angle, 
        "min_value": min_value, 
        "max_value": max_value, 
        "units": units, 
        "x": calibrated_values.x, 
        "y": calibrated_values.y, 
        "r": calibrated_values.r
      };
      let results = "Minimum Angle: " + calibration_values.min_angle + ", Maximum Angle: " + calibration_values.max_angle
      + ", Minimum Value: " + calibration_values.min_value + ", Maximum Value: " + calibration_values.max_value + ", Units: " + calibration_values.units;
      document.getElementById('calibrationResults').innerHTML = results;
      cv.imshow('canvasOutputCalibration', calibrated_values.img);
      calibrated_values.img.delete();
    });
  };

readingImgElement.onload = function() {
    let mat = cv.imread(readingImgElement);
    get_current_value(mat, calibration_values.min_angle, calibration_values.max_angle, calibration_values.min_value,
        calibration_values.max_value, calibration_values.x, calibration_values.y, calibration_values.r, )
        .then(results => {    
        console.log("Results: ", results);
        cv.imshow('canvasOutputReading', results.img);
        document.getElementById('readingResults').innerHTML = "Results: " + results.reading_value + " " + calibration_values.units;
        results.img.delete();
    });
  };

function onOpenCvReady() {
  document.getElementById('status').innerHTML = 'OpenCV.js is ready.';
}

function take_snapshot(usage){
    Webcam.snap( function(data_uri) {
        document.getElementById(usage).src = data_uri;
    } );
}