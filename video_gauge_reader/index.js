function onCvLoaded () {
    console.log('cv', cv);
    cv.onRuntimeInitialized = onReady;
}
const video = document.getElementById('video');
// video.crossOrigin = "Anonymous";
const actionBtn = document.getElementById('actionBtn');
const FPS = 30;
let stream;
let streaming = false;
let old_line = {};
let calibrated_values = {};
let final_angle = 0;
let startPoint = {"x": 0, "y": 0};

function onReady () {
    old_line = {
        "startPoint": new cv.Point(0, 0),
        "endPoint": new cv.Point(0, 0),
        "colour": new cv.Scalar(255, 255, 0)
    }
    console.log('ready');
    let src;
    let dst;
    let cap;

    video.controls = true;
    video.addEventListener('play', start);
    video.addEventListener('pause', stop);
    video.addEventListener('ended', stop);

    function start () {
        console.log('playing...');
        streaming = true;
        const width = video.videoWidth;
        const height = video.videoHeight;
        video.height = video.videoHeight;
        video.width = video.videoWidth;
        src = new cv.Mat(height, width, cv.CV_8UC4);
        dst = new cv.Mat(height, width, cv.CV_8UC1);
        cap = new cv.VideoCapture(video);
        // debugger;
        setTimeout(processVideo, 0);
    }

    function stop () {
        console.log('paused or ended');
        streaming = false;
    }

    function processVideo () {
        if (!streaming) {
            src.delete();
            dst.delete();
            return;
        }
        const begin = Date.now();
        cap.read(src)

        // cv.cvtColor(src, dst, cv.COLOR_RGBA2GRAY);
        calibrate_gauge(src, 50, 315).then(calibrated_values => {
            if (calibrated_values.circled_detected) {
                console.log("calibrated values: ", calibrated_values);
                calibration_values = {
                    "min_angle": 50,
                    "max_angle": 315,
                    "min_value": 0,
                    "max_value": 4,
                    "units": "BAR",
                    "x": calibrated_values.x,
                    "y": calibrated_values.y,
                    "r": calibrated_values.r
                };
                get_current_value(calibrated_values.img, calibration_values.min_angle, calibration_values.max_angle, calibration_values.min_value,
                    calibration_values.max_value, calibration_values.x, calibration_values.y, calibration_values.r)
                    .then(results => {
                        console.log(results);
                        if (results.reading_value != "0") {
                            // cv.line(results.img, results.line.startPoint, results.line.endPoint, results.line.colour);
                            let endP = new cv.Point((calibration_values.x + calibration_values.r*Math.sin((360 - results.final_angle) * (Math.PI / 180))), (calibration_values.y + calibration_values.r*Math.cos((360 - results.final_angle) * (Math.PI / 180))));
                            cv.line(results.img, new cv.Point(calibration_values.x, calibration_values.y), endP, results.line.colour);
                            final_angle = results.final_angle;
                            old_line = results.line;
                            old_line.endPoint = endP;
                            document.getElementById('readingResults').innerHTML = "Results: " + results.reading_value + " " + calibration_values.units;
                        }
                        else {     
                            let endP = new cv.Point((calibration_values.x + calibration_values.r * Math.sin((360 - final_angle) * (Math.PI / 180))), (calibration_values.y + calibration_values.r * Math.cos((360 - final_angle) * (Math.PI / 180))));      
                            cv.line(results.img, new cv.Point(calibration_values.x, calibration_values.y), endP, old_line.colour);
                            document.getElementById('readingResults').innerHTML = "Results: Not Read";
                        }
                        
                        cv.imshow('canvasOutput', results.img);
                        const delay = 1000/FPS - (Date.now() - begin);                    
                        setTimeout(processVideo, delay);
                        // results.img.delete();
                    });
                // cv.imshow('canvasOutput', calibrated_values.img);
                // const delay = 1000/FPS - (Date.now() - begin);
            }
            else{
                old_line = {
                    "startPoint": new cv.Point(calibration_values.x, calibration_values.y),
                    "endPoint": new cv.Point(0, 0),
                    "colour": new cv.Scalar(255, 255, 0)
                }
            }
        });        
    }
}