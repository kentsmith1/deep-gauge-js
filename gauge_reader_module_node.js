const cv = require('./opencv-4.5.5.js');
const Jimp = require('jimp');
main();


function main(){
    //Use onRunTimeInitialized to let opencv get ready
    cv['onRuntimeInitialized'] = () => {
        calibrate_gauge(1, "jpg").then(calibrated_values => {
            // console.log(calibrated_values);
            let img = new cv.Mat();
            Jimp.read("./gauge-2.jpg")
            .then(jimpSrc => {
                img = cv.matFromImageData(jimpSrc.bitmap);
                var currentReading = get_current_value(img, calibrated_values.min_angle, calibrated_values.max_angle, calibrated_values.min_value, calibrated_values.max_value,
                    calibrated_values.x, calibrated_values.y, calibrated_values.r, "1", "jpg"); 
                console.log("Current value: ", currentReading);
            }).catch(err => {
                console.log(err);
            });    
        }).catch(err => {
            console.log(err);
        });    
    }
}



function avg_circles(circles, b){
    let avg_x=0;
    let avg_y=0;
    let avg_r=0;
    for(let i=0; i < b; i++){
        avg_x += circles.data32F[3*i]; //Use the 3*i, as the JS version of the HoughCircles funcition returns the data into a 1-dimentional array
        avg_y += circles.data32F[3*i + 1];
        avg_r += circles.data32F[3*i + 2];
    }
    avg_x = parseInt(avg_x/(b));
    avg_y = parseInt(avg_y/(b));
    avg_r = parseInt(avg_r/(b));
    return {"x":avg_x, "y":avg_y, "r":avg_r};
}

function dist_2_pts(x1, y1, x2, y2){
    return Math.sqrt(Math.pow((x2 - x1),2) + Math.pow((y2 - y1),2));
}

async function calibrate_gauge(img){
    // '''
    //     This function should be run using a test image in order to calibrate the range available to the dial as well as the
    //     units.  It works by first finding the center point and radius of the gauge.  Then it draws lines at hard coded intervals
    //     (separation) in degrees.  It then prompts the user to enter position in degrees of the lowest possible value of the gauge,
    //     as well as the starting value (which is probably zero in most cases but it won't assume that).  It will then ask for the
    //     position in degrees of the largest possible value of the gauge. Finally, it will ask for the units.  This assumes that
    //     the gauge is linear (as most probably are).
    //     It will return the min value with angle in degrees (as a tuple), the max value with angle in degrees (as a tuple),
    //     and the units (as a string).
    // '''

    // let img_name = "./gauge-"+ gauge_number+"."+file_type;
    // var jimpSrc = await Jimp.read(img_name);
    // img = cv.matFromImageData(jimpSrc.bitmap);
    let height = img.size().height;
    let width = img.size().width;
    let gray = new cv.Mat();
    cv.cvtColor(img, gray, cv.COLOR_BGR2GRAY);  //convert to gray
    // gray = cv.GaussianBlur(gray, gray, (5, 5), 0)
    // gray = cv.medianBlur(gray, 5)

    //for testing, output gray image
    // cv.imwrite('gauge-%s-bw.%s' %(gauge_number, file_type),gray)

    //detect circles
    //restricting the search from 35-48% of the possible radii gives fairly good results across different samples.  Remember that
    //these are pixel values which correspond to the possible radii search range.
    let circles = new cv.Mat();
    cv.HoughCircles(gray, circles, cv.HOUGH_GRADIENT, 1, 20, 100, 50, parseInt(height*0.35), parseInt(height*0.48));
    // average found circles, found it to be more accurate than trying to tune HoughCircles parameters to get just the right one
    
    // x,y,r = avg_circles(circles, circles.cols)
    let avg_circ = avg_circles(circles, circles.cols);

    //draw center and circle
    let colour = new cv.Scalar(255, 0, 0);
    cv.circle(img, new cv.Point(avg_circ.x, avg_circ.y), avg_circ.r, colour);  // draw circle
    cv.circle(img, new cv.Point(avg_circ.x, avg_circ.y), 2, colour);  // draw center of circle

    //for testing, output circles on image
    //cv2.imwrite('gauge-%s-circles.%s' % (gauge_number, file_type), img)


    //for calibration, plot lines from center going out at every 10 degrees and add marker
    //for i from 0 to 36 (every 10 deg)

    // '''
    // goes through the motion of a circle and sets x and y values based on the set separation spacing.  Also adds text to each
    // line.  These lines and text labels serve as the reference point for the user to enter
    // NOTE: by default this approach sets 0/360 to be the +x axis (if the image has a cartesian grid in the middle), the addition
    // (i+9) in the text offset rotates the labels by 90 degrees so 0/360 is at the bottom (-y in cartesian).  So this assumes the
    // gauge is aligned in the image, but it can be adjusted by changing the value of 9 to something else.
    // '''

    let separation = 10.0; //in degrees
    let interval = parseInt(360 / separation);
    let p1 = new Array(interval);
    let p2 = new Array(interval);
    let p_text = new Array(interval);
    for(let i=0; i<interval; i++){
        p1[i] = new Array(2);
        p2[i] = new Array(2);
        p_text[i] = new Array(2);
    }
    for(let i = 0; i< interval; i++) {
        for(let j = 0; j<2; j++) {
            if (j%2==0) {
                p1[i][j] = avg_circ.x + 0.9 * avg_circ.r * Math.cos(separation * i * 3.14 / 180) //point for lines
            }
            else {
                p1[i][j] = avg_circ.y + 0.9 * avg_circ.r * Math.sin(separation * i * 3.14 / 180)
            }
        }
    }
    // console.log(p1);
    let text_offset_x = 10
    let text_offset_y = 5
    for (let i = 0; i< interval; i++) {
        for(let j = 0; j < 2; j++) {
            if (j % 2 == 0) {
                p2[i][j] = avg_circ.x + avg_circ.r * Math.cos(separation * i * 3.14 / 180)
                p_text[i][j] = avg_circ.x - text_offset_x + 1.2 * avg_circ.r * Math.cos((separation) * (i+9) * 3.14 / 180) //point for text labels, i+9 rotates the labels by 90 degrees
            }
            else {
                p2[i][j] = avg_circ.y + avg_circ.r * Math.sin(separation * i * 3.14 / 180)
                p_text[i][j] = avg_circ.y + text_offset_y + 1.2* avg_circ.r * Math.sin((separation) * (i+9) * 3.14 / 180)  // point for text labels, i+9 rotates the labels by 90 degrees
            }
        }
    }

    //add the lines and labels to the image
    for (let i = 0; i< interval; i++) {
        cv.line(img, new cv.Point(parseInt(p1[i][0]), parseInt(p1[i][1])), new cv.Point(parseInt(p2[i][0]), parseInt(p2[i][1])), new cv.Scalar(0, 255, 0), 2)
        cv.putText(img, ""+ parseInt(i*separation), new cv.Point(parseInt(p_text[i][0]), parseInt(p_text[i][1])), cv.FONT_HERSHEY_SIMPLEX, 0.3, new cv.Scalar(0,0,0), 1, cv.LINE_AA)
    }
    // cv.imwrite('gauge-1-calibration.jpg', img)
    new Jimp({
        width: img.cols,
        height: img.rows,
        data: Buffer.from(img.data)
      })
      .write('gauge-1-calibration.jpg');

    // //get user input on min, max, values, and units
    // print('gauge number: %s' %gauge_number)
    // min_angle = input('Min angle (lowest possible angle of dial) - in degrees: ') //the lowest possible angle
    // max_angle = input('Max angle (highest possible angle) - in degrees: ') //highest possible angle
    // min_value = input('Min value: ') //usually zero
    // max_value = input('Max value: ') //maximum reading of the gauge
    // units = input('Enter units: ')

    img.delete();
    gray.delete();
    circles.delete();
    //for testing purposes: hardcode and comment out raw_inputs above
    min_angle = 45
    max_angle = 320
    min_value = 0
    max_value = 200
    units = "PSI"
    return {"min_angle": min_angle, "max_angle": max_angle, "min_value": min_value, "max_value": max_value, "units": units, "x": avg_circ.x, "y": avg_circ.y, "r": avg_circ.r};
    // return min_angle, max_angle, min_value, max_value, units, x, y, r
}

async function get_current_value(img, min_angle, max_angle, min_value, max_value, x, y, r, gauge_number, file_type){

    //for testing purposes
    //img = cv2.imread('gauge-%s.%s' % (gauge_number, file_type))

    let gray2 = new cv.Mat();
    // console.log("img: ", img.data);
    cv.cvtColor(img, gray2, cv.COLOR_RGBA2GRAY);
    // console.log("gray2: ", gray2.data);
    // let gray3 = new cv.Mat();
    // cv.cvtColor(gray2, gray3, cv.COLOR_GRAY2RGBA); //Take Gray image to RGBA for JIMP
    // console.log("gray3: ", gray3.data);
    //Test JIMP
    // try{
    //     //Save Gray image for testing
    //     new Jimp({ data: Buffer.from(gray3.data), width: gray3.size().width, height: gray3.size().height }, (err, image) => {
    //         if(err){
    //             console.log(err);
    //         }
    //         else{
    //             console.log(image);
    //             image.writeAsync("guage-1-gray3.jpg")
    //             .then(resp => {
    //                 console.log(resp);
    //             })
    //             .catch(err => {
    //                 console.log(err);
    //             });
    //         }
    //       });
    // }
    // catch(err) {
    //     console.log(err);
    // }

    // Set threshold and maxValue
    let thresh = 175;
    // let thresh = 100;
    let maxValue = 255;

    // // for testing purposes, found cv2.THRESH_BINARY_INV to perform the best
    // // th, dst1 = cv2.threshold(gray2, thresh, maxValue, cv2.THRESH_BINARY);
    // // th, dst2 = cv2.threshold(gray2, thresh, maxValue, cv2.THRESH_BINARY_INV);
    // // th, dst3 = cv2.threshold(gray2, thresh, maxValue, cv2.THRESH_TRUNC);
    // // th, dst4 = cv2.threshold(gray2, thresh, maxValue, cv2.THRESH_TOZERO);
    // // th, dst5 = cv2.threshold(gray2, thresh, maxValue, cv2.THRESH_TOZERO_INV);
    // // cv2.imwrite('gauge-%s-dst1.%s' % (gauge_number, file_type), dst1)
    // // cv2.imwrite('gauge-%s-dst2.%s' % (gauge_number, file_type), dst2)
    // // cv2.imwrite('gauge-%s-dst3.%s' % (gauge_number, file_type), dst3)
    // // cv2.imwrite('gauge-%s-dst4.%s' % (gauge_number, file_type), dst4)
    // // cv2.imwrite('gauge-%s-dst5.%s' % (gauge_number, file_type), dst5)

    // // apply thresholding which helps for finding lines
    let dst2 = new cv.Mat();
    cv.threshold(gray2, dst2, thresh, maxValue, cv.THRESH_BINARY_INV);
    // console.log("dst2 data: ", dst2.data);
    // let dst3 = new cv.Mat();
    // cv.cvtColor(dst2, dst3, cv.COLOR_GRAY2RGBA); //Take Gray image to RGBA for JIMP
    // console.log("dst3 data: ", dst3.data);
    // try{
    //     //Save Gray image for testing
    //     new Jimp({ data: Buffer.from(dst3.data), width: dst3.size().width, height: dst3.size().height }, (err, image) => {
    //         if(err){
    //             console.log(err);
    //         }
    //         else{
    //             console.log(image);
    //             image.writeAsync("guage-1-dst3.jpg")
    //             .then(resp => {
    //                 console.log(resp);
    //             })
    //             .catch(err => {
    //                 console.log(err);
    //             });
    //         }
    //       });
    // }
    // catch(err) {
    //     console.log(err);
    // }

    // // found Hough Lines generally performs better without Canny / blurring, though there were a couple exceptions where it would only work with Canny / blurring
    // //dst2 = cv2.medianBlur(dst2, 5)
    // //dst2 = cv2.Canny(dst2, 50, 150)
    // //dst2 = cv2.GaussianBlur(dst2, (5, 5), 0)

    let cny = new cv.Mat(); //Canny can be used for greater edge detection
    cv.Canny(dst2, cny, 50, 100, 3);
    // // // find lines
    let minLineLength = 10; //10;
    let maxLineGap = 0; //0
    let lineThreshold = 100; //100;
    let rho = 3; //3;
    let theta = (Math.PI / 180);
    let lines = new cv.Mat();
    cv.HoughLinesP(dst2, lines, rho, theta, lineThreshold, minLineLength, maxLineGap);  // rho is set to 3 to detect more lines, easier to get more then filter them out later
       
    let color = new cv.Scalar(255, 0, 0);
    
    // // //for testing purposes, show all found lines
    // let img_line  = img.clone();
    // for (let i = 0; i < lines.rows; ++i) {
    //     let startPoint = new cv.Point(lines.data32S[i * 4], lines.data32S[i * 4 + 1]);
    //     let endPoint = new cv.Point(lines.data32S[i * 4 + 2], lines.data32S[i * 4 + 3]);
    //     cv.line(img_line, startPoint, endPoint, color);
    // }
    // try{
    //     //Save Line image for testing
    //     new Jimp({ data: Buffer.from(img_line.data), width: img_line.size().width, height: img_line.size().height }, (err, image) => {
    //         if(err){
    //             console.log(err);
    //         }
    //         else{
    //             console.log(image);
    //             image.writeAsync("guage-1-lines.jpg")
    //             .then(resp => {
    //                 console.log(resp);
    //             })
    //             .catch(err => {
    //                 console.log(err);
    //             });
    //         }
    //       });
    // }
    // catch(err) {
    //     console.log(err);
    // }

    // // remove all lines outside a given radius
    let final_line_list = [];
    //print "radius: %s" %r

    let diff1LowerBound = 0.15; //diff1LowerBound and diff1UpperBound determine how close the line should be from the center
    let diff1UpperBound = 0.30;
    let diff2LowerBound = 0.5; //diff2LowerBound and diff2UpperBound determine how close the other point of the line should be to the outside of the gauge
    let diff2UpperBound = 1.0;
    for(let i = 0; i < lines.rows; i++) {
        let x1 = lines.data32S[i*4];
        let y1 = lines.data32S[i*4 + 1];
        let x2 = lines.data32S[i*4 + 2];
        let y2 = lines.data32S[i*4 + 3];
        
        diff1 = dist_2_pts(x, y, x1, y1)  // x, y is center of circle
        diff2 = dist_2_pts(x, y, x2, y2)  // x, y is center of circle
        //set diff1 to be the smaller (closest to the center) of the two), makes the math easier
        if (diff1 > diff2){
            temp = diff1
            diff1 = diff2
            diff2 = temp
        }
        // check if line is within an acceptable range
        if ((diff1<diff1UpperBound*r) && (diff1>diff1LowerBound*r) && (diff2<diff2UpperBound*r) && (diff2>diff2LowerBound*r)){
            line_length = dist_2_pts(x1, y1, x2, y2);
            // add to final list
            final_line_list.push([x1, y1, x2, y2]);
        }
    }
    // console.log(final_line_list);

    // let img_line2  =  img.clone();
    // for (let i = 0; i < final_line_list.length; ++i) {
    //     let startPoint = new cv.Point(final_line_list[i][0], final_line_list[i][1]);
    //     let endPoint = new cv.Point(final_line_list[i][2], final_line_list[i][3]);
    //     cv.line(img_line2, startPoint, endPoint, color);
    // }
    // try{
    //     //Save Line image for testing
    //     new Jimp({ data: Buffer.from(img_line2.data), width: img_line2.size().width, height: img_line2.size().height }, (err, image) => {
    //         if(err){
    //             console.log(err);
    //         }
    //         else{
    //             console.log(image);
    //             image.writeAsync("guage-1-lines2.jpg")
    //             .then(resp => {
    //                 console.log(resp);
    //             })
    //             .catch(err => {
    //                 console.log(err);
    //             });
    //         }
    //       });
    // }
    // catch(err) {
    //     console.log(err);
    // }

    // // assumes the first line is the best one
    let x1 = final_line_list[0][0];
    let y1 = final_line_list[0][1];
    let x2 = final_line_list[0][2];
    let y2 = final_line_list[0][3];
    let startPoint = new cv.Point(x1, y1);
    let endPoint = new cv.Point(x2, y2);
    cv.line(img, startPoint, endPoint, color);
    // try{
    //     //Save Line image for testing
    //     new Jimp({ data: Buffer.from(img.data), width: img.size().width, height: img.size().height }, (err, image) => {
    //         if(err){
    //             console.log(err);
    //         }
    //         else{
    //             console.log(image);
    //             image.writeAsync("guage-1-lines3.jpg")
    //             .then(resp => {
    //                 console.log(resp);
    //             })
    //             .catch(err => {
    //                 console.log(err);
    //             });
    //         }
    //       });
    // }
    // catch(err) {
    //     console.log(err);
    // }

    // //for testing purposes, show the line overlayed on the original image
    // //cv2.imwrite('gauge-1-test.jpg', img)
    // cv2.imwrite('gauge-%s-lines-2.%s' % (gauge_number, file_type), img)

    //find the farthest point from the center to be what is used to determine the angle
    let dist_pt_0 = dist_2_pts(x, y, x1, y1);
    let dist_pt_1 = dist_2_pts(x, y, x2, y2);
    let x_angle = 0;
    let y_angle = 0;
    if (dist_pt_0 > dist_pt_1) {
        x_angle = x1 - x;
        y_angle = y - y1;
    }
    else {
        x_angle = x2 - x;
        y_angle = y - y2;
    }
    // take the arc tan of y/x to find the angle
    let res = Math.atan(y_angle / x_angle);
    // console.log("res: ", res);
    // //np.rad2deg(res) //coverts to degrees

    // // print x_angle
    // // print y_angle
    // // print res
    // // print np.rad2deg(res)

    // //these were determined by trial and error
    res = res * (180 / Math.PI);
    let final_angle = 0;
    if(x_angle > 0 && y_angle > 0){  //in quadrant I
        final_angle = 270 - res;
    }
    if(x_angle < 0 && y_angle > 0) { //in quadrant II
        final_angle = 90 - res;
    }
    if(x_angle < 0 && y_angle < 0) {  //in quadrant III
        final_angle = 90 - res;
    }
    if(x_angle > 0 && y_angle < 0){  //in quadrant IV
        final_angle = 270 - res;
    }
    // console.log("final_angle: ", final_angle);
    //print final_angle

    let old_min = min_angle;
    let old_max = max_angle;
    // console.log("old values ", old_min, old_max);
    let new_min = min_value;
    let new_max = max_value;
    // console.log("new values ", new_min, new_max);
    let old_value = final_angle;

    let old_range = (old_max - old_min);
    let new_range = (new_max - new_min)
    let new_value = (((old_value - old_min) * new_range) / old_range) + new_min
    // console.log("old_range: ", old_range);
    // console.log("new_range: ", new_range);
    // console.log("new_value: ", new_value);
    return new_value
}